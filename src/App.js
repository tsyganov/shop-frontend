import React from 'react';

const App = () => (
  <>
    <header>Navbar will go here</header>
    <main>Main content will go here</main>
  </>
);

export default App;
